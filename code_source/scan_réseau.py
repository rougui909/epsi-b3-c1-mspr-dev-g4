import nmap
import socket
from tkinter import *


def scan_ports(ip, start_port, end_port):
    """Scanne les ports d'une adresse IP donnée."""
    open_ports = []
    for port in range(start_port, end_port + 1):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(0.1)
        result = sock.connect_ex((ip, port))
        if result == 0:
            open_ports.append(port)
        sock.close()
    return open_ports


def scan_reseau_et_ports(reseau, resultats_text):
    """Fait un scan de réseau et pour chaque IP découverte, fait un scan de port."""
    nm = nmap.PortScanner()
    nm.scan(reseau, arguments="-sP")
    resultats = ""
    for host in nm.all_hosts()[:-1]:
        resultats += "Adresse IP : " + host + "\n"
        open_ports = scan_ports(host, 1, 1024)
        resultats += "Les ports ouverts sont : " + str(open_ports) + "\n\n"
    resultats_text.configure(state="normal")
    resultats_text.delete("1.0", END)
    resultats_text.insert("1.0", resultats)
    resultats_text.configure(state="disabled")


