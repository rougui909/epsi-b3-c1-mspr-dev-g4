
import nmap
import requests
import socket
import netifaces


# Fonction pour récupérer l'adresse IP de la machine
def get_ip_address():
    hostname = socket.gethostname()
    ip = socket.gethostbyname(hostname)
    return ip

# Fonction pour récupérer le nom de la machine
def get_hostname():
    hostname = socket.gethostname()
    return hostname

# Fonction pour récupérer l'adresse IP publique de la machine
def get_public_ip():
    st = requests.get("https://api.ipify.org").text
    return st

# Fonction pour récupérer l'état de la connexion Internet
def get_internet_status():
    try:
        socket.create_connection(("www.google.com", 80))
        return "Connected"
    except OSError:
        pass
    return "Not Connected"

# Fonction pour récuperer le nom dynamique 

def get_name_ddns():
    name = socket.getfqdn(get_ip_address())
    return name

# Fonction pour récupérer la liste des machines détectées sur le réseau

def get_network_devices():
    nm = nmap.PortScanner()
    nm.scan(hosts='192.168.31.0/24', arguments='-n -sP')

    resultats = ""
    for host in nm.all_hosts()[:-1]:
        
        resultats +=  host + "\n"
    return resultats
